.. _site-configuration:

Setup and site configuration
============================

This section describes how to setup `pglift` on target host and explains
various aspects of deployment operations.

.. toctree::
    :maxdepth: 1

    settings
    config-templates
    postgresql-configuration
    postgresql-authentication
    systemd
    pgbackrest
    monitoring
    patroni
    rsyslog
    logrotate
