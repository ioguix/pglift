Roles
=====

.. currentmodule:: pglift.roles

Module :mod:`pglift.roles` exposes the following API to manipulate
PostgreSQL roles:

.. autofunction:: apply
.. autofunction:: exists
.. autofunction:: get
.. autofunction:: drop
