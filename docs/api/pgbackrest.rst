pgbackrest
==========

Module :mod:`pglift.pgbackrest` exposes the following API functions for backup
management using pgBackRest_:

.. currentmodule:: pglift.pgbackrest

.. autofunction:: iter_backups
.. autofunction:: restore

.. _pgBackRest: https://pgbackrest.org/
